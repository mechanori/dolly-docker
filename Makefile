include make_env

build:
	docker build \
		-t $(CONTAINER_NAME) \
		-f Dockerfile.template \
		--build-arg ROS_DISTRO=$(ROS_DISTRO) .

run:
	sudo xhost +local:root && \
	docker run \
		--rm \
		-it \
		--gpus all \
		-v /tmp/.X11-unix:/tmp/.X11-unix \
		-e DISPLAY=${DISPLAY} \
		-e QT_X11_NO_MITSHM=1 \
		-e ROS_DISTRO=$(ROS_DISTRO) \
		-e DOLLY_EXAMPLE=$(DOLLY_EXAMPLE) \
		$(CONTAINER_NAME)
