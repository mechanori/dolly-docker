# dolly-docker

Run the Dolly ROS 2 demo in a Docker container with NVIDIA GPU acceleration.

## Install

### Docker + NVIDIA Container Toolkit

#### Arch
```
yay -S docker nvidia-container-toolkit
```

#### Ubuntu
```
distribution=$(. /etc/os-release;echo $ID$VERSION_ID) && \
  curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add - && \
  curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list && \
  sudo apt update && sudo apt install -y docker.io nvidia-docker2
```
#### Enable and Restart Docker
```
sudo systemctl enable docker && sudo systemctl start docker
```

## Build and Run
```
make build && make run
```

## References
- Dolly
  - https://github.com/chapulina/dolly
- ROS 2 (Robot Operating System)
  - https://index.ros.org/doc/ros2/
- Docker
  - https://docs.docker.com/
- NVIDIA Container Toolkit
  - https://github.com/NVIDIA/nvidia-docker
  - https://wiki.archlinux.org/index.php/Docker#Run_GPU_accelerated_Docker_containers_with_NVIDIA_GPUs
