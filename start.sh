#!/bin/bash

# Source ROS2 setup
. /opt/ros/${ROS_DISTRO}/setup.bash
. /usr/share/gazebo/setup.sh
. /ws/install/setup.bash

# start dolly empty world example
ros2 launch dolly_gazebo dolly.launch.py world:=${DOLLY_EXAMPLE}
